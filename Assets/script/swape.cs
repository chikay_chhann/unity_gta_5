﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swape : MonoBehaviour
{
    public static int index_player_show = 0;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            movement.change_char_or_view = true;
            //SetActive(false) on current charactor 
            this.transform.GetChild(index_player_show).gameObject.SetActive(false);

            //SetActive(true) on next charactor 
            index_player_show++;
            //if max index => 0
            if(index_player_show == this.transform.childCount) index_player_show = 0;
            this.transform.GetChild(index_player_show).gameObject.SetActive(true);
        }
    }
}
