﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class blink_eye : MonoBehaviour
{
    public float speed_open_eye = 75;
    public float speed_close_eye = 150;
    private float mix_num = 255;
    private float min_num = 0;
    private float num = 0.5f;
    private bool once_true = true;

    // Start is called before the first frame update
    void Start()
    {
        model_static_var_for_spawn.is_open_eye = true;
        model_static_var_for_spawn.allow_open_eye = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (model_static_var_for_spawn.allow_open_eye)
        {
            if (model_static_var_for_spawn.is_open_eye)
            {
                if (once_true)
                {
                    num = mix_num;
                    once_true = false;
                }
                num -= speed_open_eye * Time.deltaTime;
                if (num < min_num)
                {
                    num = min_num;
                    once_true = true;
                    model_static_var_for_spawn.allow_open_eye = false;
                }
                GetComponent<Image>().color = new Color32(0, 0, 0, (byte)num);
            }
            else
            {
                if (once_true)
                {
                    num = min_num;
                    once_true = false;
                }
                num += speed_close_eye * Time.deltaTime;
                if (num > mix_num)
                {
                    num = mix_num;
                    once_true = true;
                    model_static_var_for_spawn.allow_open_eye = false;
                }
                GetComponent<Image>().color = new Color32(0, 0, 0, (byte)num);
            }
        }
    }
}
