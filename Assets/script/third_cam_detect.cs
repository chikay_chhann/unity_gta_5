﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class third_cam_detect : MonoBehaviour
{
    public Transform t_third_p;
    public Transform t_cam_p;
    public Transform t_cam_right_line;
    public Transform t_cam_left_line;
    public Transform t_first_p;
    public float cam_half_degree = 34;
    public float speed_change_first_p_pos = 30;
    public float speed_increase_cam_pos = 5;
    public float mix_between_camera_n_player = 10f;
    public float min_between_camera_n_player = 1.0f;
    public float distant_between_camera_n_third = 0.25f;
    private float mix_hit;
    private float between_camera_n_player;
    private float temp_player_y_degree;
    private float temp_cam_p;

    private void Start()
    {
        temp_cam_p = mix_between_camera_n_player - distant_between_camera_n_third;
        between_camera_n_player = mix_between_camera_n_player;
        mix_hit = Mathf.Abs(between_camera_n_player / Mathf.Cos(cam_half_degree));
        temp_player_y_degree = this.transform.localEulerAngles.y;

        //set anis_z
        float first_p_z = Mathf.Cos((temp_player_y_degree * Mathf.PI) / 180) * mix_between_camera_n_player;

        //set anis_x
        float first_p_x = Mathf.Sin((temp_player_y_degree * Mathf.PI) / 180) * mix_between_camera_n_player;

        //change position and icrease z position higher between line and obj
        Vector3 pos_obj = new Vector3(first_p_x + this.transform.position.x, t_third_p.transform.position.y, first_p_z + this.transform.position.z);
        t_third_p.transform.position = pos_obj;
    }

    private void FixedUpdate()
    {
        if (!model_static_var_for_spawn.first_per)
        {
            RaycastHit right_hit;
            RaycastHit left_hit;

            if (Physics.Raycast(t_cam_right_line.position, t_cam_right_line.TransformDirection(Vector3.forward), out right_hit, mix_hit, ~0, QueryTriggerInteraction.Ignore)
                || Physics.Raycast(t_cam_left_line.position, t_cam_left_line.TransformDirection(Vector3.forward), out left_hit, mix_hit, ~0, QueryTriggerInteraction.Ignore))
            {
                if (min_between_camera_n_player < between_camera_n_player)
                {
                    between_camera_n_player -= speed_change_first_p_pos * Time.deltaTime;
                    temp_player_y_degree = this.transform.localEulerAngles.y;

                    mix_hit = Mathf.Abs(between_camera_n_player / Mathf.Cos(cam_half_degree));

                    //set anis_z
                    float first_p_z = Mathf.Cos((temp_player_y_degree * Mathf.PI) / 180) * between_camera_n_player;

                    //set anis_x
                    float first_p_x = Mathf.Sin((temp_player_y_degree * Mathf.PI) / 180) * between_camera_n_player;

                    //change position and icrease z position higher between line and obj
                    //Vector3 pos_obj = new Vector3(first_p_x, t_first_p.transform.position.y, first_p_z);
                    Vector3 pos_obj = new Vector3(first_p_x + this.transform.position.x, t_third_p.transform.position.y, first_p_z + this.transform.position.z);
                    t_third_p.transform.position = pos_obj;
                }
            }
            else
            {
                if (mix_between_camera_n_player > between_camera_n_player && (temp_player_y_degree != this.transform.localEulerAngles.y || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
                {
                    between_camera_n_player += speed_change_first_p_pos * Time.deltaTime;
                    if (between_camera_n_player > mix_between_camera_n_player)
                    {
                        between_camera_n_player = mix_between_camera_n_player;
                    }
                    mix_hit = Mathf.Abs(between_camera_n_player / Mathf.Cos(cam_half_degree));
                    //set anis_z
                    float first_p_z = Mathf.Cos((this.transform.localEulerAngles.y * Mathf.PI) / 180) * between_camera_n_player;

                    //set anis_x
                    float first_p_x = Mathf.Sin((this.transform.localEulerAngles.y * Mathf.PI) / 180) * between_camera_n_player;

                    //change position and icrease z position higher between line and obj
                    //Vector3 pos_obj = new Vector3(first_p_x, t_first_p.transform.position.y, first_p_z);
                    Vector3 pos_obj = new Vector3(first_p_x + this.transform.position.x, t_third_p.transform.position.y, first_p_z + this.transform.position.z);
                    t_third_p.transform.position = pos_obj;
                }
            }

            if (Physics.Raycast(t_cam_right_line.position, t_cam_right_line.TransformDirection(Vector3.forward), out right_hit, mix_hit))
            {
                Debug.DrawRay(t_cam_right_line.position, t_cam_right_line.TransformDirection(Vector3.forward) * right_hit.distance, Color.yellow);
            }
            else
            {
                Debug.DrawRay(t_cam_right_line.position, t_cam_right_line.TransformDirection(Vector3.forward) * mix_hit, Color.white);
            }

            if (Physics.Raycast(t_cam_left_line.position, t_cam_left_line.TransformDirection(Vector3.forward), out left_hit, mix_hit))
            {
                Debug.DrawRay(t_cam_left_line.position, t_cam_left_line.TransformDirection(Vector3.forward) * left_hit.distance, Color.yellow);
            }
            else
            {
                Debug.DrawRay(t_cam_left_line.position, t_cam_left_line.TransformDirection(Vector3.forward) * mix_hit, Color.white);
            }

            if (temp_cam_p > between_camera_n_player - distant_between_camera_n_third)
            {
                temp_cam_p = between_camera_n_player - distant_between_camera_n_third;
            }
            else
            {
                temp_cam_p += speed_increase_cam_pos * Time.deltaTime;
                if (temp_cam_p > between_camera_n_player - distant_between_camera_n_third)
                {
                    temp_cam_p = between_camera_n_player - distant_between_camera_n_third;
                }
            }

            //set anis_z
            float cam_z = Mathf.Cos((this.transform.localEulerAngles.y * Mathf.PI) / 180) * temp_cam_p;

            //set anis_x
            float cam_x = Mathf.Sin((this.transform.localEulerAngles.y * Mathf.PI) / 180) * temp_cam_p;

            //change position and icrease z position higher between line and obj
            //Vector3 pos_cam = new Vector3(cam_x, t_first_p.transform.position.y, cam_z);
            Vector3 pos_cam = new Vector3(cam_x + this.transform.position.x, t_third_p.transform.position.y, cam_z + this.transform.position.z);
            t_cam_p.position = pos_cam;

        }
    }
}
