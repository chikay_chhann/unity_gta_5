﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emun_position_player_show_list : MonoBehaviour
{
    public enum enum_position_player_show
    {
        stair_up_front,
        stair_down_front,
        stair_up_behind,
        stair_down_behind,
        room_x01_front_door,
        room_x01_behind_door,
        room_x02_front_door,
        room_x02_behind_door,
        room_x02_2_behind_door,
        room_x03_front_door,
        room_x03_behind_door,
        room_x03_2_behind_door,
        room_x04_front_door,
        room_x04_behind_door,
        room_x05_behind_door,
        room_x05_2_behind_door,
        room_x05_front_door,
        room_x05_2_front_door,
        room_x06_behind_door,
        room_x06_2_behind_door,
        room_x06_front_door,
        room_x06_2_front_door,
        room_x07_front_door,
        room_x08_behind_door,
        boy_toilet,
        girl_toilet,
    };
}
