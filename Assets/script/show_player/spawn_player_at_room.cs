﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_player_at_room : MonoBehaviour
{
    public Transform room_x01_front_door = null;
    public Transform room_x01_behind_door = null;
    public Transform room_x02_front_door = null;
    public Transform room_x02_behind_door = null;
    public Transform room_x02_2_behind_door = null;
    public Transform room_x03_front_door = null;
    public Transform room_x03_behind_door = null;
    public Transform room_x03_2_behind_door = null;
    public Transform room_x04_front_door = null;
    public Transform room_x04_behind_door = null;
    public Transform room_x05_behind_door = null;
    public Transform room_x05_2_behind_door = null;
    public Transform room_x05_front_door = null;
    public Transform room_x05_2_front_door = null;
    public Transform room_x06_behind_door = null;
    public Transform room_x06_2_behind_door = null;
    public Transform room_x06_front_door = null;
    public Transform room_x06_2_front_door = null;
    public Transform room_x07_front_door = null;
    public Transform room_x08_behind_door = null;
    public Transform boy_toilet = null;
    public Transform girl_toilet = null;
    // Start is called before the first frame update

    void room(Transform t, string s){
        if (model_static_var_for_spawn.static_name_position == s)
        {
            this.transform.position = t.position;
            this.transform.GetChild(0).rotation = t.rotation;
            this.transform.GetChild(1).rotation = t.rotation;
            //this.transform.rotation = t.rotation;
        }
    }
    void Start()
    {
        room(room_x01_front_door, emun_position_player_show_list.enum_position_player_show.room_x01_front_door.ToString());
        room(room_x01_behind_door, emun_position_player_show_list.enum_position_player_show.room_x01_behind_door.ToString());
        
        room(room_x02_front_door, emun_position_player_show_list.enum_position_player_show.room_x02_front_door.ToString());
        room(room_x02_behind_door, emun_position_player_show_list.enum_position_player_show.room_x02_behind_door.ToString());
        room(room_x02_2_behind_door, emun_position_player_show_list.enum_position_player_show.room_x02_2_behind_door.ToString());

        room(room_x03_front_door, emun_position_player_show_list.enum_position_player_show.room_x03_front_door.ToString());
        room(room_x03_behind_door, emun_position_player_show_list.enum_position_player_show.room_x03_behind_door.ToString());
        room(room_x03_2_behind_door, emun_position_player_show_list.enum_position_player_show.room_x03_2_behind_door.ToString());

        room(room_x04_front_door, emun_position_player_show_list.enum_position_player_show.room_x04_front_door.ToString());
        room(room_x04_behind_door, emun_position_player_show_list.enum_position_player_show.room_x04_behind_door.ToString());

        room(room_x05_behind_door, emun_position_player_show_list.enum_position_player_show.room_x05_behind_door.ToString());
        room(room_x05_2_behind_door, emun_position_player_show_list.enum_position_player_show.room_x05_2_behind_door.ToString());
        room(room_x05_front_door, emun_position_player_show_list.enum_position_player_show.room_x05_front_door.ToString());
        room(room_x05_2_front_door, emun_position_player_show_list.enum_position_player_show.room_x05_2_front_door.ToString());
        
        room(room_x06_behind_door, emun_position_player_show_list.enum_position_player_show.room_x06_behind_door.ToString());
        room(room_x06_2_behind_door, emun_position_player_show_list.enum_position_player_show.room_x06_2_behind_door.ToString());
        room(room_x06_front_door, emun_position_player_show_list.enum_position_player_show.room_x06_front_door.ToString());
        room(room_x06_2_front_door, emun_position_player_show_list.enum_position_player_show.room_x06_2_front_door.ToString());
        
        room(room_x07_front_door, emun_position_player_show_list.enum_position_player_show.room_x07_front_door.ToString());

        room(room_x08_behind_door, emun_position_player_show_list.enum_position_player_show.room_x08_behind_door.ToString());

        room(boy_toilet, emun_position_player_show_list.enum_position_player_show.boy_toilet.ToString());
        room(girl_toilet, emun_position_player_show_list.enum_position_player_show.girl_toilet.ToString());
        
    }
}
