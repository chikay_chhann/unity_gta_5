﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller_eye_animation : MonoBehaviour
{
    public Animator animator_eye_top;
    public Animator animator_eye_buttom;
    private float inc_num = 0.0f;

    private void Update()
    {
        if (model_static_var_for_spawn.is_open_eye)
        {
            if (inc_num < 1)
            {
                inc_num += Time.deltaTime;
            }
            else
            {
                inc_num = 1;
            }
            animator_eye_top.SetFloat("eye_open_size", inc_num);
            animator_eye_buttom.SetFloat("eye_open_size", inc_num);
        }
    }
}
