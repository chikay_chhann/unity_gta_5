﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class text_open_door : MonoBehaviour
{
    bool is_trigger = false;
    public GameObject text;
    public float delay_log_new_scene = 2;
    public int sense_idx;
    public emun_position_player_show_list.enum_position_player_show name_position = new emun_position_player_show_list.enum_position_player_show();
    void Start()
    {
        text.SetActive(false);
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return) && is_trigger)
        {
            model_static_var_for_spawn.static_name_position = name_position.ToString();
            model_static_var_for_spawn.is_open_eye = false;
            model_static_var_for_spawn.allow_open_eye = true;
            StartCoroutine(delay_change_sense());
        }
    }
    IEnumerator delay_change_sense()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(delay_log_new_scene);
        model_static_var_for_spawn.is_open_eye = true;
        model_static_var_for_spawn.allow_open_eye = false;
        SceneManager.LoadScene(sense_idx);
    }
    private void OnTriggerEnter(Collider other)
    {
        text.SetActive(true);
        is_trigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
        is_trigger = false;
    }
}
