﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spawn_player_at_stair : MonoBehaviour
{
    public Transform stair_up_front = null;
    public Transform stair_down_behind = null;
    public Transform stair_down_front = null;
    public Transform stair_up_behind = null;


    // Start is called before the first frame update
    void Start()
    {
        /*if (SceneManager.GetActiveScene().name == "ground_first_floor")
        {
            if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_up_front.ToString())
            {
                this.transform.position = stair_up_front.position;
                this.transform.GetChild(0).rotation = stair_up_front.rotation;
                this.transform.GetChild(1).rotation = stair_up_front.rotation;
            }
            else if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_up_behind.ToString())
            {
                this.transform.position = stair_up_behind.position;
                this.transform.GetChild(0).rotation = stair_up_behind.rotation;
                this.transform.GetChild(1).rotation = stair_up_behind.rotation;
            }
        }
        else
        {*/
            if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_up_front.ToString())
            {
                this.transform.position = stair_up_front.position;
                this.transform.GetChild(0).rotation = stair_up_front.rotation;
                this.transform.GetChild(1).rotation = stair_up_front.rotation;
            }
            else if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_down_front.ToString())
            {
                this.transform.position = stair_down_front.position;
                this.transform.GetChild(0).rotation = stair_down_front.rotation;
                this.transform.GetChild(1).rotation = stair_down_front.rotation;
                //this.transform.rotation = stair_down_front.rotation;
            }
            else if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_up_behind.ToString())
            {
                this.transform.position = stair_up_behind.position;
                this.transform.GetChild(0).rotation = stair_up_behind.rotation;
                this.transform.GetChild(1).rotation = stair_up_behind.rotation;
                //this.transform.rotation = stair_up_behind.rotation;
            }
            else if (model_static_var_for_spawn.static_name_position == emun_position_player_show_list.enum_position_player_show.stair_down_behind.ToString())
            {
                this.transform.position = stair_down_behind.position;
                this.transform.GetChild(0).rotation = stair_down_behind.rotation;
                this.transform.GetChild(1).rotation = stair_down_behind.rotation;
                //this.transform.rotation = stair_down_behind.rotation;
            }
       /* }*/
    }
}
