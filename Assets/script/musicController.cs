using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicController : MonoBehaviour
{
    public static musicController instance;
    public bool playing;
    public AudioSource musicSoure;
    public AudioClip[] audioClips;
    // Start is called before the first frame update
 
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        playing = true;
        StartCoroutine(playMusicLoop());
    }

    IEnumerator playMusicLoop()
    {
        yield return null;

        while (playing)
        {
            for(int i = 0; i < audioClips.Length; i++)
            {
                musicSoure.clip = audioClips[i];

                musicSoure.Play();

                while (musicSoure.isPlaying)
                {
                    yield return null;
                }
            }
        }
    }
}
